/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

//  1. Retrieve data for ids : [2, 13, 23].
const fs = require("fs");
const path = require("path");

fs.readFile(path.join(__dirname, './data.json'), (err, data) => {
    if (err) {
        console.error(err);
    } else {
        console.log("Employess Data read from data.json");
        let employeesData = JSON.parse(data);
        let employeesDataValues = Object.values(employeesData).flat();
        let employeeIDSData = employeesDataValues.filter((employee) => {
            if (employee.id === 2 || employee.id === 13 || employee.id === 23) {
                return employee;
            }
        })
        fs.writeFile(path.join(__dirname, './retrivedData.json'), JSON.stringify(employeeIDSData), (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log("Retrived data for ids :[2, 13, 23]");
                let groupedBasedOnCompanies = employeesDataValues.reduce((acc, employee) => {
                    if (acc[employee.company] === undefined) {
                        acc[employee.company] = [employee];
                    } else {
                        acc[employee.company].push(employee);
                    }
                    return acc;
                }, {})
                fs.writeFile(path.join(__dirname, './groupedBasedOnCompanies.json'), JSON.stringify(groupedBasedOnCompanies), "utf-8", (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log("Completed Grouping based on companies.");
                        let Powerpuff_Brigade = groupedBasedOnCompanies["Powerpuff Brigade"];
                        fs.writeFile(path.join(__dirname, './Powerpuff-Brigade-data.json'), JSON.stringify(Powerpuff_Brigade), (err) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log("Retrived Powerpuff Brigade company data.");
                                let ID = 2;
                                let deleteId2 = employeesDataValues.filter((employee) => {
                                    if (employee.id !== ID) {
                                        return employee;
                                    }
                                });
                                fs.writeFile(path.join(__dirname, './deleteId2.json'), JSON.stringify(deleteId2), (err) => {
                                    if (err) {
                                        console.error(err);
                                    } else {
                                        console.log("Deleted entry with id 2");
                                        let sortedBasedOnCompany = deleteId2.sort((employee1, employee2) => {
                                            if (employee1.company < employee2.company) {
                                                return 1;
                                            } else if (employee1.company === employee2.company) {
                                                if (employee1.id < employee2.id) {
                                                    return 1;
                                                } else {
                                                    return -1;
                                                }
                                            } else {
                                                return -1;
                                            }
                                        })
                                        fs.writeFile(path.join(__dirname, './sortedBasedOnCompanies.json'), JSON.stringify(sortedBasedOnCompany), (err) => {
                                            if (err) {
                                                console.error(err);
                                            } else {
                                                console.log("Sorted data based on companies");
                                                let index92 = 0;
                                                let index93 = 0;
                                                let value92 = 0;
                                                let value93 = 0;
                                                let swapData = sortedBasedOnCompany;
                                                let index1 = sortedBasedOnCompany.findIndex((employee, index) => {
                                                    if (employee.id === 92) {
                                                        index92 = index;
                                                        value92 = employee;
                                                    }
                                                    if (employee.id === 93) {
                                                        index93 = index;
                                                        value93 = employee;
                                                    }
                                                });
                                                swapData[index92] = value93;
                                                swapData[index93] = value92;
                                                fs.writeFile(path.join(__dirname, './swapData.json'), JSON.stringify(swapData), (err) => {
                                                    if (err) {
                                                        console.error(err);
                                                    } else {
                                                        console.log("Data with id 92 and 93 are swapped.");
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})

