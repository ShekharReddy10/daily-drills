const logins = [{ "id": 1, "first_name": "Valera", "last_name": "Pinsent", "email": "vpinsent0@google.co.jp", "gender": "Male", "ip_address": "253.171.63.171" },
{ "id": 2, "first_name": "Kenneth", "last_name": "Hinemoor", "email": "khinemoor1@yellowbook.com", "gender": "Polygender", "ip_address": "50.231.58.150" },
{ "id": 3, "first_name": "Roman", "last_name": "Sedcole", "email": "rsedcole2@addtoany.com", "gender": "Genderqueer", "ip_address": "236.52.184.83" },
{ "id": 4, "first_name": "Lind", "last_name": "Ladyman", "email": "lladyman3@wordpress.org", "gender": "Male", "ip_address": "118.12.213.144" },
{ "id": 5, "first_name": "Jocelyne", "last_name": "Casse", "email": "jcasse4@ehow.com", "gender": "Agender", "ip_address": "176.202.254.113" },
{ "id": 6, "first_name": "Stafford", "last_name": "Dandy", "email": "sdandy5@exblog.jp", "gender": "Female", "ip_address": "111.139.161.143" },
{ "id": 7, "first_name": "Jeramey", "last_name": "Sweetsur", "email": "jsweetsur6@youtube.com", "gender": "Genderqueer", "ip_address": "196.247.246.106" },
{ "id": 8, "first_name": "Anna-diane", "last_name": "Wingar", "email": "awingar7@auda.org.au", "gender": "Agender", "ip_address": "148.229.65.98" },
{ "id": 9, "first_name": "Cherianne", "last_name": "Rantoul", "email": "crantoul8@craigslist.org", "gender": "Genderfluid", "ip_address": "141.40.134.234" },
{ "id": 10, "first_name": "Nico", "last_name": "Dunstall", "email": "ndunstall9@technorati.com", "gender": "Female", "ip_address": "37.12.213.144" }]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/
// 1. Find all people who are Agender.

const agender = logins.filter((gender) => {
    if (gender.gender === 'Agender') {
        return gender.first_name;
    }
})
console.log(agender);

//2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143].
const ipaddress = logins.map((address) => {
    return address.ip_address.split('.');
})
console.log(ipaddress);

// 3. Find the sum of all the second components of the ip addresses.
const secondComponent = logins.map((address) => {
    return address.ip_address.split('.');
})
    .reduce((acc, curr) => {
        return acc + parseInt(curr[1]);
    }, 0)
console.log(secondComponent);

//4. Find the sum of all the fourth components of the ip addresses.
const fourthComponent = logins.map((address) => {
    return address.ip_address.split('.');
})
    .reduce((acc, curr) => {
        return acc + parseInt(curr[3]);
    }, 0)
console.log(fourthComponent);

//5. Compute the full name of each person and store it in a new key (full_name or something) for each person.
logins.map((name) => {
    if (name.full_name === undefined) {
        return name.full_name = name.first_name+" "+name.last_name;
    }
})
console.log(logins);

// 6. Filter out all the .org emails.
const orgEmails = logins.map((mails) => {
    return mails.email;
})
    .filter((org) => {
        return org.includes(".org");
    })
console.log(orgEmails);

// 7. Calculate how many .org, .au, .com emails are there.
const countEmails = logins.reduce((acc,curr) => {
    if(curr.email.includes(".org") || curr.email.includes(".au") || curr.email.includes(".com")){
        acc +=1;
    }
    return acc;
},0)
console.log(countEmails);

// 8. Sort the data in descending order of first name.
const sortNames = logins.sort((name1,name2) => {
    if(name1.first_name < name2.first_name){
        return 1;
    }else{
        return -1
    }
});
console.log(sortNames);