
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following.

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
// 1. Fetch all the users.
fetch('https://jsonplaceholder.typicode.com/users')
    .then((userResponse) => {
        return userResponse.json();
    })
    .then((users) => {
       console.log(users);
    })
    .catch((err) => {
        console.error(err);
    })

// // 2. Fetch all the todos.
fetch('https://jsonplaceholder.typicode.com/todos')
    .then((todosResponse) => {
        return todosResponse.json();
    })
    .then((todos) => {
        console.log(todos);
    })
    .catch((err) => {
        console.error(err);
    })

// 3. Use the promise chain and fetch the users first and then the todos.

fetch('https://jsonplaceholder.typicode.com/users')
    .then((userResponse) => {
        console.log("Users data is fetched.")
        return userResponse.json();
    })
    .then((users) => {
        console.log(users);
        return fetch('https://jsonplaceholder.typicode.com/todos');
    })
    .then((todosResponse) => {
        console.log("Todos data is fetched");
        return todosResponse.json();
    })
    .then((todos) => {
        console.log(todos);
    })
    .catch((err) => {
        console.error(err);
    })

// 4. Use the promise chain and fetch the users first and then all the details for each user.

fetch('https://jsonplaceholder.typicode.com/users')
    .then((userResponse) => {
        console.log("Users data is fetched.")
        return userResponse.json();
    })
    .then((userDetails) => {
        console.log("User details are fetched");
        let userIds = userDetails.map((user) => {
            return user.id;
        })
            .map((userId) => {
                return `https://jsonplaceholder.typicode.com/users?id=${userId}`
            })
        return Promise.all(userIds.map((url) => {
            return fetch(url)
        }))
    })
    .then((responses) => {
        console.log("responses");
        return Promise.all(responses.map((response) => {
            return response.json();
        }))
    })
    .then((details) => {
       // console.log(details);
    })
    .catch((err) => {
        console.err(err);
    })

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo.

fetch('https://jsonplaceholder.typicode.com/todos')
    .then((todosResponse) => {
        return todosResponse.json();
    })
    .then((todos) => {
        let userId = todos[0].userId;
        return fetch(`https://jsonplaceholder.typicode.com/users?id=${userId}`)
    })
    .then((todosUserDetails) => {
        console.log("User details for first todo is fetched");
        return todosUserDetails.json();
    })
    .then((userDetails) => {
        console.log(userDetails);
    })
    .catch((err) => {
        console.error(err);
    })

