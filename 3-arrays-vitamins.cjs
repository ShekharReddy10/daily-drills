const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

// 1. Get all items that are available.

const itemAvailable = items.filter((item) => {
    if (item.available === true) {
        return item;
    }
})
console.log(itemAvailable);

// 2. Get all items containing only Vitamin C.
const onlyVitaminC = items.filter((item) => {
    if (item.contains === "Vitamin C") {
        return item;
    }
})
console.log(onlyVitaminC);

// 3. Get all items containing Vitamin A.
const vitaminA = items.filter((item) => {
    if (item.contains.includes("Vitamin A")) {
        return item;
    }
})
console.log(vitaminA);

// 4.  Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }
const vitamins = items.reduce((acc, curr) => {
    itemArray = curr.contains.split(", ");
    itemArray.map((vitamin) => {
        if (curr.contains.includes(vitamin) && acc[vitamin] === undefined) {
            acc[vitamin] = [curr.name];
        } else {
            if (curr.contains.includes(vitamin)) {
                acc[vitamin].push(curr.name);
            }
        }
    })
    return acc;
}, {})
console.log(vitamins);

// 5. Sort items based on number of Vitamins they contain.
const sortable = items.sort((name1, name2) => {
    name2Length = name2.contains.split(", ").length;
    name1Length = name1.contains.split(", ").length;
    if (name2Length > name1Length) {
        return 1;
    } else if (name2Length === name1Length) {
        if (name2.name < name1.name) { // if contains same number of vitamins then compare by names.
            return 1;
        } else {
            return -1;
        }
    }
    else {
        return -1;
    }
})
console.log(sortable);