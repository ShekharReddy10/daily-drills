/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file.
    login(user, 3).then((user) => {
        console.log("Login Success");
        return getData();
    })
    .catch((error) => {
        console.error("Login Failure",error);
    })
    .then(() => {
        console.log("GetData Success");
    })
    .catch(() => {
        console.log("GetData Failure");
    })
    .then(() => {

    })
}
logData("Shekhar" , "do");
/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

const path = require("path");
const fs = require("fs");
function createFiles() {
    let indexArray = Array.from({ length: 2 }, (_, index) => index);
    let filesArray = indexArray.map((index) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, `./file${index + 1}.json`), JSON.stringify({ a: 1, b: 2 }), (err) => {
                if (err) {
                    reject("File creation failed");
                } else {
                    console.log(`file${index + 1}.json is created`);
                    resolve(`file${index + 1}.json`);
                }
            })
        })
    })
    return Promise.all(filesArray);
}
function deleteFiles() {
    let indexArray = Array.from({ length: 2 }, (_, index) => index);
    let filesArray = indexArray.map((index) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                fs.unlink(path.join(__dirname, `./file${index + 1}.json`), (err) => {
                    if (err) {
                        reject("File deletion failed");
                    } else {
                        console.log(`file${index + 1}.json is deleted`);
                        resolve(`file${index + 1}.json`);
                    }
                })
            }, 2 * 1000)
        })
    })
    return Promise.all(filesArray);
}

createFiles()
    .then(() => {
        return deleteFiles();
    }).catch((err) => {
        console.error(err);
    })


// 2.

return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, './lipsum.txt'), (err, data) => {
        if (err) {
            reject("File read failed");
        } else {
            resolve(data);
        }
    })
})
    .then((data) => {
        console.log("Done reading lipsum file");
        return new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, './lipsumCopy.txt'), data.toString(), (err, data) => {
                if (err) {
                    reject("lipsumCopy.txt File written failed");
                } else {
                    resolve(data);
                }
            })
        })
    })
    .then(() => {
        console.log("Done writing in to new file named lipsumCopy.txt")
        return new Promise((resolve, reject) => {
            fs.unlink(path.join(__dirname, './lipsum.txt'), (err, data) => {
                if (err) {
                    reject("lipsum.txt File deletion failed");
                } else {
                    console.log("Original file lipsum.txt is deleted.");
                    resolve(data);
                }
            })
        })
    })
    .catch((err) => {
        console.error(err);
    })