const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

// 1. Find all users who are interested in playing video games.
// 
const VideoGames = Object.entries(users).filter((userDetails) => {
    array = userDetails[1].interests;
    if (array[0].includes("Video Games")) {
        return userDetails;
    }
}).map((user) => {
    return user[0];
})
console.log(VideoGames);

// 2. Find all users staying in Germany.
const germanyUsers = Object.entries(users).filter((userDetails) => {
    if (userDetails[1].nationality === "Germany") {
        return userDetails;
    }
}).map((user) => {
    return user[0];
})
console.log(germanyUsers);

// 3. Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10.
const Senior = 3;
const Developer = 2;
const Intern = 1;
const sortUsers = Object.entries(users).filter((userDetails) => {
  userDetails.sort((name1, name2) => {
    //console.log(name1);
  })  
})
//console.log(sortUsers);

// 4. Find all users with masters Degree.
const masterDegreeUsers = Object.entries(users).filter((userDetails) => {
    if (userDetails[1].qualification.includes("Master")) {
        return userDetails;
    }
}).map((user) => {
    return user[0];
})
console.log(masterDegreeUsers);