const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/

// Q1. Find all the movies with total earnings more than $500M.
const earnings = Object.entries(favouritesMovies).filter((movie) => {
    if (parseInt(movie[1].totalEarnings.substring(1)) > 500) {
      return movie[1];
    }
});
//console.log(Object.fromEntries(earnings));

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const nominee = Object.entries(favouritesMovies).filter((movie) => {
    if (parseInt(movie[1].totalEarnings.substring(1)) > 500 && movie[1].oscarNominations >3) {
      return movie[1];
    }
});
//console.log(Object.fromEntries(nominee));

// Q.3 Find all movies of the actor "Leonardo Dicaprio".
const actorLeonardoDicaprio = Object.entries(favouritesMovies).filter((movie) => {
    actorsArray = movie[1].actors;
    if (actorsArray.includes("Leonardo Dicaprio")) {
      return movie[1];
    }
});
//console.log(Object.fromEntries(actorLeonardoDicaprio));

// //  Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.

const sort = Object.entries(favouritesMovies).sort(([,movie1],[, movie2]) => {
    if (movie1.imdbRating < movie2.imdbRating) {
      return 1;
    }else if(movie1.imdbRating === movie2.imdbRating){
        if(parseInt(movie1.totalEarnings.substring(1)) < parseInt(movie2.totalEarnings.substring(1))){
            return 1;
        }else{
            return -1;
        }
    }else{
        return -1;
    }
});
//console.log(Object.fromEntries(sort));

// //  Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime
let genre = {
    "drama": 1,
    "sci-fi" : 1,
    "adventure" : 1,
    "thriller" : 1,
    "crime" : 1
}
const genres = Object.entries(favouritesMovies).reduce((acc, curr) => {
    if(acc[curr[0]]=== undefined){
        if(curr[1].genre.includes("drama")){
            genre["drama"[curr[0]]] = acc[curr];
        }
        else if(curr[1].genre.includes("sci-fi")){
            genre ["sci-fi"] = acc[curr];
        }
        else if(curr[1].genre.includes("adventure")){
            genre.adventure[curr[0]] = acc[curr];
        }
        else if(curr[1].genre.includes("thriller")){
            genre.thriller[curr[0]] = acc[curr];
        }
        else{
            genre.crime[curr[0]] = acc[curr];
        }
        
    }
    return acc;
},{})
console.log(genre);

