const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],
    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

// Q1. Find all the items with price more than $65.
let productObject = products.pop();
function priceGreaterThan65(productObject) {
  let price = Object.entries(productObject).filter((product) => {
    if (Array.isArray(product[1])) {
      product[1].forEach((pro) => {
        return priceGreaterThan65(pro);
      });
    } else {
      if (parseInt(product[1].price.slice(1)) > 65) {
        return product;
      }
    }
  });
  // console.log(Object.fromEntries(price));
}
priceGreaterThan65(productObject);

// Q2. Find all the items where quantity ordered is more than 1.

function quantityMoreThan1(productObject) {
  let quantity = Object.entries(productObject).filter((product) => {
    if (Array.isArray(product[1])) {
      product[1].forEach((pro) => {
        return quantityMoreThan1(pro);
      });
    } else {
      if (product[1].quantity > 1) {
        return product;
      }
    }
  });
 //console.log(Object.fromEntries(quantity));
}
quantityMoreThan1(productObject);

// Q.3 Get all items which are mentioned as fragile.
function fragileTypeItems(productObject) {
    let fragileItem = Object.entries(productObject).filter((product) => {
      if (Array.isArray(product[1])) {
        product[1].forEach((pro) => {
          return fragileTypeItems(pro);
        });
      } else {
        if (product[1].type !== undefined && product[1].type ==='fragile') {
          return product;
        }
      }
    });
   console.log(Object.fromEntries(fragileItem));
  }
  fragileTypeItems(productObject);
  